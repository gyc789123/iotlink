INSERT INTO `iotdb`.`sys_job`(`job_id`, `job_name`, `job_group`, `invoke_target`, `cron_expression`, `misfire_policy`, `concurrent`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '用量补偿-所有-近三天', 'POLLING', 'compensateSendTask.pollingFlow(30,2,\'\',0,10000)', '0 0 20 * * ? *', '1', '1', '0', 'admin', '2022-10-24 11:12:31', 'admin', '2023-06-02 15:16:28', '');
INSERT INTO `iotdb`.`sys_job`(`job_id`, `job_name`, `job_group`, `invoke_target`, `cron_expression`, `misfire_policy`, `concurrent`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '用量补偿-正常', 'POLLING', 'compensateSendTask.pollingFlow(30,0,\'4\',0,5000)', '0 0/18 4-20 * * ? *', '1', '1', '0', 'admin', '2022-10-24 10:56:44', 'admin', '2023-03-27 08:44:33', '');
INSERT INTO `iotdb`.`sys_job`(`job_id`, `job_name`, `job_group`, `invoke_target`, `cron_expression`, `misfire_policy`, `concurrent`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '订单请求API重试', 'SYSTEM', 'orderCardApiRequestTask.Execution', '0 0/1 * * * ?', '1', '1', '0', 'admin', '2022-10-20 10:52:44', '', '2022-10-21 12:03:30', '');
INSERT INTO `iotdb`.`sys_job`(`job_id`, `job_name`, `job_group`, `invoke_target`, `cron_expression`, `misfire_policy`, `concurrent`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '资费计划-卡数量同步', 'SYSTEM', 'cardPacketCountTask.SynPacketCount', '45 0 0 * * ? *', '1', '1', '0', 'admin', '2022-07-20 10:45:44', '', '2022-07-20 17:49:28', '');


alter table yz_order
 add column is_audi smallint(5) NOT NULL DEFAULT '1' COMMENT '是否已审核通过 0否 1 是',
   add column callback_address varchar(500) DEFAULT NULL COMMENT '回调地址',
 add column  callback_status smallint(5) NOT NULL DEFAULT '0' COMMENT '回调是否成功',
  add column callback_time datetime DEFAULT NULL COMMENT '最近一次回调执行时间',
  add column callback_info varchar(1000) DEFAULT NULL COMMENT '回调返回消息',
  add column is_dd_status smallint(5) DEFAULT NULL COMMENT '是否订购达量断网',
  add column is_dd_exCount int(3) NOT NULL DEFAULT '0' COMMENT '达量断网-执行次数',
  add column is_dd_quota varchar(20) DEFAULT NULL COMMENT '达量断网执行时阈值',
  add column is_dd_exTime datetime DEFAULT NULL COMMENT '订购达量断网 执行时间',
 add column	is_dd_exStatus smallint(5) DEFAULT NULL COMMENT '订购达量断网 执行结果',
  add column is_boot_status smallint(5) DEFAULT NULL COMMENT '是否 开机',
  add column is_boot_exTime datetime DEFAULT NULL COMMENT '开机 执行时间',
  add column is_boot_exStatus smallint(5) DEFAULT NULL COMMENT '开机 执行结果',
  add column is_boot_exCount int(3) NOT NULL DEFAULT '0' COMMENT '开机-执行次数',
  add column is_openNet_status smallint(5) DEFAULT NULL COMMENT '是否 开网',
  add column is_openNet_exTime datetime DEFAULT NULL COMMENT '开网 执行时间',
 add column  is_openNet_exStatus smallint(5) DEFAULT NULL COMMENT '开网 执行结果',
  add column is_openNet_exCount int(3) NOT NULL DEFAULT '0' COMMENT '开网-执行次数';


alter table yz_agent_packet
    add column
        is_dd smallint(5) DEFAULT NULL COMMENT '是否订购达量断网';

INSERT INTO `iotdb`.`sys_dict_data`(`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, 3, '接口', 'api', 'yunze_order_creType', NULL, NULL, 'N', '0', 'admin', '2022-10-08 11:02:57', '', NULL, NULL);


alter table  yz_order add column is_audi smallint(5) NOT NULL DEFAULT '1' COMMENT '是否已审核通过 0否 1 是';
alter table  yz_indexpage add column api_pending_order int(11) NOT NULL DEFAULT '0' COMMENT '待审核订单数量-平台使用API申请';
DROP TABLE IF EXISTS `yz_xsgl_customer`;
CREATE TABLE `yz_xsgl_customer`  (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `c_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公司名称',
                                     `dept_id` int(11) NOT NULL COMMENT '代理编号 与 agent_id 一致 但需要用于系统结构 数据验证',
                                     `grade_id` int(11) NOT NULL COMMENT '客户等级',
                                     `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '省',
                                     `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '市',
                                     `district` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '区',
                                     `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
                                     `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
                                     `modified_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
                                     `one_category_id` smallint(5) UNSIGNED NOT NULL COMMENT '一级分类ID',
                                     `two_category_id` smallint(5) UNSIGNED NOT NULL COMMENT '二级分类ID',
                                     `remarks` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                     `source_id` smallint(5) NOT NULL COMMENT '来源类型',
                                     `is_Recycle` smallint(2) NOT NULL COMMENT '是否回收',
                                     `affiliation_id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '客户所属合伙人',
                                     `tax_number` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '税号',
                                     `Co_Discount` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '优惠折扣',
                                     `state_id` smallint(5) NOT NULL COMMENT '客户状态',
                                     `sales_id` int(11) NOT NULL COMMENT '所属销售id',
                                     `affiliation_rate` double(6, 2) NOT NULL DEFAULT 0.00 COMMENT '合伙人费率',
  `sales_rate` double(6, 2) NOT NULL DEFAULT 0.00 COMMENT '销售费率',
  `afterSale_rate` double(6, 2) NOT NULL DEFAULT 0.00 COMMENT '售后费率',
  `credit_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '统一社会信用代码',
  `id_card` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '身份证',
  `sort_id` smallint(5) NOT NULL DEFAULT 1 COMMENT '客户类型 1.企业 2个人',
  `alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '别名用于对外展示',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dept_idUNIQUE`(`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 495 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '客户表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
DROP TABLE IF EXISTS `yz_xsgl_contract`;
CREATE TABLE `yz_xsgl_contract`  (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '合同编号',
                                     `c_id` int(11) NOT NULL COMMENT '客户编号',
                                     `c_name` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '客户名称',
                                     `c_fatId` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '销售单位id',
                                     `c_fatName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '销售单位名称',
                                     `c_userId` int(11) NOT NULL COMMENT '销售id',
                                     `c_userName` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '销售名称',
                                     `c_brId` smallint(5) NOT NULL COMMENT '开票要求',
                                     `c_state` smallint(5) NOT NULL COMMENT '合同状态',
                                     `c_total_original_price` double(10, 2) NOT NULL COMMENT '合计原价',
  `c_transportation_costs` double(6, 2) NOT NULL COMMENT '运费',
  `c_total_unit_price` double(10, 2) NOT NULL COMMENT '单价合计',
  `c_contract_amount` double(10, 2) NOT NULL COMMENT '合同金额',
  `c_other_amount` double(10, 2) NOT NULL COMMENT '其他金额',
  `c_the_actual_amount` double(10, 2) NOT NULL COMMENT '实际金额',
  `c_discount` double(10, 2) NOT NULL COMMENT '折扣',
  `c_party_a_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '甲方代表',
  `c_party_a_phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '甲方代表电话',
  `c_party_a_address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '甲方地址',
  `c_party_b_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '已方代表id',
  `c_party_b_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '已方代表',
  `c_party_b_phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '已方代表电话',
  `c_party_b_address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '已方地址',
  `modified_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `receive_payment` smallint(5) NOT NULL COMMENT '收款状态',
  `remarks` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `profit_sharing` tinyint(4) NOT NULL COMMENT '分润模式',
  `state` tinyint(4) NOT NULL COMMENT '状态',
  `c_time` date NOT NULL COMMENT '合同日期',
  `c_discountl_price` double(10, 2) NOT NULL COMMENT '优惠金额',
  `c_total_tariff` double(10, 2) NOT NULL COMMENT '资费合计',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code_un`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '销售合同' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_contract_other
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_contract_other`;
CREATE TABLE `yz_xsgl_contract_other`  (
                                           `Cor_ID` int(11) NOT NULL AUTO_INCREMENT,
                                           `Cor_CyName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
                                           `Cor_CyUnit` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
                                           `Cor_Univalent` double NOT NULL COMMENT '单价',
                                           `Cor_Quantity` int(11) NOT NULL COMMENT '数量',
                                           `Cor_Amount` double NOT NULL COMMENT '金额',
                                           `Cor_Remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                           `Cor_CtID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属合同表编号',
                                           `Cor_Category` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
                                           `Cor_ModelAndNumber` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '型号规格',
                                           PRIMARY KEY (`Cor_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同信息其他表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_contract_standard
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_contract_standard`;
CREATE TABLE `yz_xsgl_contract_standard`  (
                                              `Csd_ID` int(11) NOT NULL AUTO_INCREMENT,
                                              `Csd_CyID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品ID',
                                              `Csd_CyName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
                                              `Csd_CyUnit` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单位',
                                              `Csd_Original_Price` double(10, 2) NULL DEFAULT NULL COMMENT '原价',
  `Csd_Univalent` double NOT NULL COMMENT '单价',
  `Csd_Quantity` int(11) NOT NULL COMMENT '数量',
  `Csd_Amount` double NOT NULL COMMENT '金额',
  `Csd_Discount` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '折扣',
  `Csd_Remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `Csd_CtID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属合同表编号',
  `Csd_Sort` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品分类',
  `Csd_Category` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品类别',
  `Csd_ModelAndNumber` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '型号规格',
  `state` tinyint(4) NOT NULL COMMENT '状态',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`Csd_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同信息标配表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_contract_tariff
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_contract_tariff`;
CREATE TABLE `yz_xsgl_contract_tariff`  (
                                            `ctf_id` int(11) NOT NULL AUTO_INCREMENT,
                                            `ctf_packet_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资费计划id',
                                            `ctf_package_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资费组id',
                                            `ctf_packet_price` double(10, 2) NOT NULL COMMENT '资费计划成本',
  `ctf_quantity` int(11) NOT NULL COMMENT '数量',
  `ctf_suggested_price` double(10, 2) NOT NULL COMMENT '建议售价》资费售价',
  `ctf_remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctf_ctId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属合同表编号',
  `ctf_packet_agent_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资费计划名称',
  `ctf_package_agent_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资费组名称',
  `ctf_parameter` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资费计划详细参数数据',
  `ctf_unit_price` double(10, 2) NOT NULL COMMENT '单价》最终售价',
  `ctf_discount` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '折扣',
  `ctf_amount` double(10, 2) NOT NULL COMMENT '总金额',
  `ctf_packet_flow` double(10, 2) NOT NULL DEFAULT 0.00 COMMENT '套餐包流量',
  `ctf_packet_valid_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '月' COMMENT '周期单位',
  `ctf_packet_valid_time` int(20) NOT NULL COMMENT '周期',
  `ctf_packet_valid_type` smallint(5) NOT NULL COMMENT '资费计划生效类型 当月生效 yunze_card_takeEffect_type',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `state` tinyint(4) NOT NULL COMMENT '状态',
  PRIMARY KEY (`ctf_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同信息-资费-表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_customer_account
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_customer_account`;
CREATE TABLE `yz_xsgl_customer_account`  (
                                             `Cat_ID` int(11) NOT NULL AUTO_INCREMENT,
                                             `Cat_Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '开户行账号名称',
                                             `Cat_Bank_Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行名称',
                                             `Cat_Number` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行卡号',
                                             `Cat_CoID` int(11) NOT NULL COMMENT '所属供客户编号',
                                             `Cat_User_Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名称',
                                             `Cat_Zip_Code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮编',
                                             `Cat_Address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
                                             `Cat_atId` smallint(5) NOT NULL COMMENT '1.开票地址\r\n2.发货地址\r\n3.其他地址',
                                             `state` smallint(2) NOT NULL DEFAULT 1 COMMENT '状态 0 删除',
                                             PRIMARY KEY (`Cat_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 919 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户账号信息表（银行卡）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_customer_call_log
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_customer_call_log`;
CREATE TABLE `yz_xsgl_customer_call_log`  (
                                              `CCG_ID` int(11) NOT NULL AUTO_INCREMENT,
                                              `CCG_CoID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户ID',
                                              `CCG_CsfName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户联系人姓名',
                                              `CCG_CsfPhone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户联系人电话',
                                              `CCG_Time` datetime(0) NOT NULL COMMENT '联系时间',
                                              `CCG_CtID` int(11) NOT NULL COMMENT '联系方式',
                                              `CCG_UserID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '销售id',
                                              `CCG_Issue` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '议题',
                                              `CCG_Content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
                                              `CCG_Remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                              PRIMARY KEY (`CCG_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户联系记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_customer_change
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_customer_change`;
CREATE TABLE `yz_xsgl_customer_change`  (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `dept_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
                                            `ctype` int(2) NOT NULL COMMENT '变更类型',
                                            `cbefore` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '变更前',
                                            `cafterward` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '变更后',
                                            `createTime` datetime(0) NOT NULL COMMENT '创建时间',
                                            `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                                            `execution_status` int(2) NOT NULL COMMENT '执行状态',
                                            `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人id',
                                            `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人姓名',
                                            `request_args` longblob NOT NULL COMMENT '请求参数',
                                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '客户敏感信息变更记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_customer_img
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_customer_img`;
CREATE TABLE `yz_xsgl_customer_img`  (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `dept_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '集团ID',
                                         `cu_desc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片描述',
                                         `pic_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片URL',
                                         `is_master` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否主图：0.非主图1.主图',
                                         `pic_order` tinyint(4) NOT NULL DEFAULT 0 COMMENT '图片排序',
                                         `pic_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '图片是否有效：0无效 1有效',
                                         `modified_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
                                         `type` smallint(5) NOT NULL COMMENT '图片类型',
                                         PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '客户图片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_customer_people
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_customer_people`;
CREATE TABLE `yz_xsgl_customer_people`  (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `cu_id` int(11) NOT NULL COMMENT '所属客户id',
                                            `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
                                            `telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
                                            `qq` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'QQ',
                                            `nailed` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '钉钉\r\n',
                                            `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
                                            `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                            `job_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '职务',
                                            `state` smallint(5) NOT NULL COMMENT '状态',
                                            `is_maste` smallint(5) NOT NULL COMMENT '是否主要联系人',
                                            `gender_id` smallint(5) NOT NULL COMMENT '性别',
                                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 384 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '客户联系人' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_xsgl_customer_people_img
-- ----------------------------
DROP TABLE IF EXISTS `yz_xsgl_customer_people_img`;
CREATE TABLE `yz_xsgl_customer_people_img`  (
                                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                                `cup_id` int(10) UNSIGNED NOT NULL COMMENT '联系人ID',
                                                `cup_desc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片描述',
                                                `pic_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片URL',
                                                `is_master` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否主图：0.非主图1.主图',
                                                `pic_order` tinyint(4) NOT NULL DEFAULT 0 COMMENT '图片排序',
                                                `pic_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '图片是否有效：0无效 1有效',
                                                `modified_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
                                                `dept_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '集团id',
                                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '客户联系人图片\r\n' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
DROP TABLE IF EXISTS `cwgl_deposit_info_details`;
CREATE TABLE `cwgl_deposit_info_details`  (
                                              `Dids_ID` int(11) NOT NULL AUTO_INCREMENT,
                                              `Dids_Time` date NOT NULL COMMENT '入款日期',
                                              `Dids_FatName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司账户',
                                              `Dids_Amount` double NOT NULL COMMENT '金额',
                                              `Dids_Remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                              `Dids_DioID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属入款信息ID',
                                              PRIMARY KEY (`Dids_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4394 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '入款信息详情表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
